import subprocess
import sys


def test_output_func_counts(runpy, code_dir):
    sls = code_dir.joinpath("tests", "sls", "output.sls")
    cmd = [sys.executable, runpy, "state", sls]
    ret = subprocess.run(cmd, stdout=subprocess.PIPE)
    assert b"nop: 3 successful" in ret.stdout
    assert b"fail_without_changes: 2 failed" in ret.stdout
    assert b"anop: 1 successful" in ret.stdout
    assert b"present: 2 created successfully" in ret.stdout
    assert b"present: 1 updated successfully" in ret.stdout
    assert b"present: 1 no-op" in ret.stdout
    assert b"absent: 2 no-op" in ret.stdout

    assert ret.returncode == 0
