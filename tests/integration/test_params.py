import json
import subprocess
import sys
from typing import Dict

import pytest
from pytest_idem.runner import IdemRunException
from pytest_idem.runner import run_sls


def test_params():
    run_ret = run_sls(["success"], params=["params"], sls_offset="sls/params")
    assert run_ret
    assert isinstance(run_ret, dict), run_ret
    assert len(run_ret) == 1, "Expecting 1 state"
    for state, ret in run_ret.items():
        assert ret["result"] is True, state
        assert ret["name"] == "Some State"
        assert ret["new_state"]
        assert ret["new_state"]["location"] == "eastus"
        assert ret["new_state"]["backup_location"] == "westus"
        assert ret["new_state"]["empty"] is None
        assert ret["new_state"]["empty_str"] == "_str"
        assert ret["new_state"]["include_1"] == "include_1"
        assert ret["new_state"]["include_2"] == "include_2"
        assert ret["new_state"]["non_exists_default_val"] == "default_val"


def test_params_failure(hub):
    with pytest.raises(IdemRunException) as e:
        ret = run_sls(["failure"], params="params", sls_offset="sls/params")
        assert (
            "Error rendering sls: : Jinja variable 'dict object' has no attribute 'invalid'"
            == str(e)
        )


def test_params_absent():
    run_ret = run_sls(["noparams"], sls_offset="sls/params")
    assert run_ret
    assert isinstance(run_ret, dict), run_ret
    assert len(run_ret) == 3, "Expecting 3 states"
    for state, ret in run_ret.items():
        assert ret["result"] is True, state
        assert ret["new_state"]
        assert not ret["new_state"]["resource_id"]


def test_different_params_present():
    run_ret = run_sls(["noparams"], params=["params"], sls_offset="sls/params")
    assert run_ret
    assert isinstance(run_ret, dict), run_ret
    assert len(run_ret) == 3, "Expecting 3 states"
    for state, ret in run_ret.items():
        assert ret["result"] is True, state
        assert ret["new_state"]
        if ret["name"] == "vpc":
            assert ret["new_state"]["resource_id"] == "dummy"
        else:
            assert not ret["new_state"]["resource_id"]


def test_params_cli(runpy, tests_dir):
    sls_dir = tests_dir / "sls"
    cmd = [
        sys.executable,
        runpy,
        "state",
        f"{sls_dir / 'params' / 'success.sls'}",
        "--param-sources",
        f"file://{sls_dir / 'params' /'params.sls' }",
        "--params",
        f"{sls_dir / 'params' / 'params.sls'}",
        "--output=json",
    ]
    ret = subprocess.run(cmd, capture_output=True, encoding="utf-8", env={})
    assert ret.returncode == 0, ret.stderr
    data = json.loads(ret.stdout)
    state_id = "test_|-State Some State Present_|-Some State_|-present"
    assert data[state_id]["__run_num"] == 1
    assert data[state_id]["new_state"] == {
        "backup_location": "westus",
        "empty": None,
        "empty_str": "_str",
        "include_1": "include_1",
        "include_2": "include_2",
        "location": "eastus",
        "non_exists_default_val": "default_val",
    }
    assert data[state_id]["name"] == "Some State"
    assert data[state_id]["result"] is True


def test_params_cli_implicit(runpy, tests_dir):
    sls_dir = tests_dir / "sls"
    cmd = [
        sys.executable,
        runpy,
        "state",
        f"{sls_dir / 'params' / 'success.sls'}",
        "--param-sources",
        f"file://{sls_dir / 'params' }",
        "--params",
        "params",
        "--output=json",
        "--esm-plugin=null",
    ]
    ret = subprocess.run(cmd, capture_output=True, encoding="utf-8", env={})
    assert ret.returncode == 0, ret.stderr

    data = json.loads(ret.stdout)
    state_id = "test_|-State Some State Present_|-Some State_|-present"
    assert data[state_id]["__run_num"] == 1
    assert data[state_id]["new_state"] == {
        "backup_location": "westus",
        "empty": None,
        "empty_str": "_str",
        "include_1": "include_1",
        "include_2": "include_2",
        "location": "eastus",
        "non_exists_default_val": "default_val",
    }
    assert data[state_id]["name"] == "Some State"
    assert data[state_id]["result"] is True


def test_invalid_yaml_syntax_in_params(runpy, tests_dir):
    sls_dir = tests_dir / "sls"
    cmd = [
        sys.executable,
        runpy,
        "state",
        f"{sls_dir / 'params' / 'failure.sls'}",
        "--param-sources",
        f"file://{sls_dir / 'params' }",
        "--params",
        "invalid_params",
        "--output=json",
    ]
    ret = subprocess.run(cmd, capture_output=True, encoding="utf-8", env={})
    assert ret.returncode == 1, ret.stderr
    assert "Error while parsing" in ret.stderr
    assert "invalid_params" in ret.stderr


def test_params_override_later_include_effective():
    ret = run_sls(["params_override"], params=["params_1"], sls_offset="sls/params")
    assert ret
    assert isinstance(ret, Dict), ret
    assert len(ret) == 1, "Expecting 1 state"
    for state, ret in ret.items():
        assert ret["result"] is True, state
        assert ret["name"] == "Some State"
        assert ret["new_state"]
        assert ret["new_state"]["a"] == 1
        assert ret["new_state"]["b"] == 1.2


def test_params_override_later_include_effective_reverse():
    ret = run_sls(["params_override"], params=["params_2"], sls_offset="sls/params")
    assert ret
    assert isinstance(ret, Dict), ret
    assert len(ret) == 1, "Expecting 1 state"
    for state, ret in ret.items():
        assert ret["result"] is True, state
        assert ret["name"] == "Some State"
        assert ret["new_state"]
        assert ret["new_state"]["a"] == 2
        assert ret["new_state"]["b"] == 1.1


def test_params_override_multiple_files():
    ret = run_sls(
        ["params_override"],
        params=["params_1", "params_2"],
        sls_offset="sls/params",
    )
    assert ret
    assert isinstance(ret, Dict), ret
    assert len(ret) == 1, "Expecting 1 state"
    for state, ret in ret.items():
        assert ret["result"] is True, state
        assert ret["name"] == "Some State"
        assert ret["new_state"]
        assert ret["new_state"]["a"] == 2
        assert ret["new_state"]["b"] == 1.1


def test_params_override_multiple_files_reverse():
    ret = run_sls(
        ["params_override"],
        params=["params_2", "params_1"],
        sls_offset="sls/params",
    )
    assert ret
    assert isinstance(ret, Dict), ret
    assert len(ret) == 1, "Expecting 1 state"
    for state, ret in ret.items():
        assert ret["result"] is True, state
        assert ret["name"] == "Some State"
        assert ret["new_state"]
        assert ret["new_state"]["a"] == 1
        assert ret["new_state"]["b"] == 1.2
