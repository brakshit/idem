from pytest_idem.runner import run_sls


def test_normal(hub):
    ret = run_sls(["success"], sls_offset="sls/invert")
    assert ret
    assert len(ret) == 4
    assert "test_|-first_|-first_|-nop" in ret
    assert "test_|-second_|-second_|-nop" in ret
    assert "test_|-third_|-third_|-nop" in ret
    assert "test_|-any_order_|-any_order_|-nop" in ret

    run_first = ret["test_|-first_|-first_|-nop"]["__run_num"]
    run_second = ret["test_|-second_|-second_|-nop"]["__run_num"]
    run_third = ret["test_|-third_|-third_|-nop"]["__run_num"]
    assert run_first < run_second
    assert run_second < run_third


def test_invert(hub):
    ret = run_sls(["success"], sls_offset="sls/invert", invert_state=True)
    assert ret
    assert len(ret) == 4
    assert "test_|-first_|-first_|-nop" in ret
    assert "test_|-second_|-second_|-nop" in ret
    assert "test_|-third_|-third_|-nop" in ret
    assert "test_|-any_order_|-any_order_|-nop" in ret

    run_first = ret["test_|-first_|-first_|-nop"]["__run_num"]
    run_second = ret["test_|-second_|-second_|-nop"]["__run_num"]
    run_third = ret["test_|-third_|-third_|-nop"]["__run_num"]
    assert run_first > run_second
    assert run_second > run_third
