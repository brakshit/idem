from pytest_idem.runner import run_yaml_block


def test_exec_nonexistant_state_path(hub):
    """
    Test for failure when the given path doesn't exist
    """
    STATE = """
    exec_state:
      exec.run:
        - path: bad.path
    """
    ret = run_yaml_block(STATE)

    result = next(iter(ret.values()))

    assert result["result"] is False
    assert result["new_state"] is None
    assert "AttributeError: 'exec' has no attribute 'bad'" in result["comment"]


def test_exec_failure(hub):
    """
    Test an exec module that fails
    """
    STATE = """
    exec_state:
      exec.run:
        - path: test.fail
    """
    ret = run_yaml_block(STATE)

    result = next(iter(ret.values()))

    assert result["result"] is False
    assert "Exception: Expected failure" in result["comment"]


def test_doc_example(hub):
    """
    Test the example of this function used in the docs
    """
    STATE = """
    exec_func:
      exec.run:
        - path: test.more
        - acct_profile: default
        - args:
          - arg1
          - arg2
          - arg3
        - kwargs:
            kwarg_1: val_1
            kwarg_2: val_2
            kwarg_3: val_3
    """
    acct_data = {"profiles": {"test": {"default": {"key": "value"}}}}
    ret = run_yaml_block(STATE, acct_data=acct_data)

    result = next(iter(ret.values()))

    assert result["result"] is True
    assert result["new_state"] == {
        "args": ("arg1", "arg2", "arg3"),
        "ctx": {
            "acct": {"key": "value"},
            "acct_details": {"account_id": None},
            "test": False,
        },
        "kwargs": {"kwarg_1": "val_1", "kwarg_2": "val_2", "kwarg_3": "val_3"},
    }


def test_arg_bind(hub):
    """
    Verify that an exec module can be used with arg bind
    """
    STATE = """
    provides_args:
      exec.run:
        - path: test.more
        - args:
          - arg1
          - arg2
          - arg3
        - kwargs:
            kwarg_1: val_1
            kwarg_2: val_2
            kwarg_3: val_3
    uses_args:
      test.present:
        - new_state: ${exec:provides_args:kwargs}
    """
    ret = run_yaml_block(STATE)

    state_provides_args = ret["exec_|-provides_args_|-provides_args_|-run"]
    assert state_provides_args["result"] is True
    assert state_provides_args["new_state"] == {
        "args": ("arg1", "arg2", "arg3"),
        "ctx": {"acct": {}, "acct_details": {"account_id": None}, "test": False},
        "kwargs": {"kwarg_1": "val_1", "kwarg_2": "val_2", "kwarg_3": "val_3"},
    }

    state_uses_argbind = ret["test_|-uses_args_|-uses_args_|-present"]
    assert state_uses_argbind["result"] is True, state_uses_argbind["comment"]
    assert state_uses_argbind["new_state"] == {
        "kwarg_1": "val_1",
        "kwarg_2": "val_2",
        "kwarg_3": "val_3",
    }


def test_exec_state_path(hub):
    """
    Test that an exec module can be run from the state
    """
    STATE = """
    exec_state:
      exec.run:
        - path: test.ping
    """
    ret = run_yaml_block(STATE)

    result = next(iter(ret.values()))

    assert result["result"] is True
    assert result["new_state"] is True


def test_exec_state_test(hub):
    """
    Test that an exec module with "test" flag is executed
    """
    STATE = """
    exec_state:
      exec.run:
        - path: test.ping
    """
    ret = run_yaml_block(STATE, test=True)

    result = next(iter(ret.values()))

    assert result["result"] is True
    assert result["new_state"] is True
    assert result["comment"] == ["idem exec test.ping --acct-profile=default"]


def test_exec_state_name(hub):
    """
    Test that an exec module can be run from the state with the path as the name
    """
    STATE = """
    test.ping:
      exec.run
    """
    ret = run_yaml_block(STATE)

    result = next(iter(ret.values()))

    assert result["result"] is True
    assert result["new_state"] is True
    assert result["comment"] == ["idem exec test.ping --acct-profile=default"]


def test_exec_state_ctx(hub):
    """
    Test that an exec module can be run from the state with a profile specified in the state
    """
    STATE = """
    exec_state:
      exec.run:
        - path: test.ctx
        - acct_profile: test_profile
    """
    acct_data = {"profiles": {"test": {"test_profile": {"key": "value"}}}}
    ret = run_yaml_block(STATE, acct_data=acct_data)

    result = next(iter(ret.values()))

    assert result["result"] is True
    assert result["new_state"] == {
        "acct": {"key": "value"},
        "acct_details": {"account_id": None},
        "test": False,
    }
    assert result["comment"] == ["idem exec test.ctx --acct-profile=test_profile"]


def test_exec_state_ctx_default(hub):
    """
    Test that an exec module can be run from the state using a default profile
    """
    STATE = """
    exec_state:
      exec.run:
        - path: test.ctx
    """
    acct_data = {"profiles": {"test": {"default": {"key": "value"}}}}
    ret = run_yaml_block(STATE, acct_data=acct_data)

    result = next(iter(ret.values()))

    assert result["result"] is True
    assert result["new_state"] == {
        "acct": {"key": "value"},
        "acct_details": {"account_id": None},
        "test": False,
    }
    assert result["comment"] == ["idem exec test.ctx --acct-profile=default"]


def test_exec_state_ctx_acct_data(hub):
    """
    Test that an exec module can be run from the state with acct_data specified in the state
    """
    STATE = """
    exec_state:
      exec.run:
        - path: test.ctx
        - acct_data: {"profiles": {"test": {"default": {"key": "value"}}}}
    """
    ret = run_yaml_block(STATE)

    result = next(iter(ret.values()))

    assert result["result"] is True
    assert result["new_state"] == {
        "acct": {"key": "value"},
        "acct_details": {"account_id": None},
        "test": False,
    }
    assert result["comment"] == ["idem exec test.ctx --acct-profile=default"]
