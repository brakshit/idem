import subprocess
import sys


def test_sls_shell(tree, runpy):
    cmd = [sys.executable, runpy, "state", "simple", "--sls-sources", f"file://{tree}"]
    ret = subprocess.run(cmd, stdout=subprocess.PIPE)
    assert b"True" in ret.stdout
    assert b"Success!" in ret.stdout
    assert b"Failure!" in ret.stdout
    assert b"happy" in ret.stdout
    assert b"sad" in ret.stdout
    assert ret.returncode == 0


def test_sls_implied_source(runpy, code_dir):
    sls = code_dir.joinpath("tests", "sls", "simple.sls")
    cmd = [sys.executable, runpy, "state", sls]
    ret = subprocess.run(cmd, stdout=subprocess.PIPE)
    assert b"True" in ret.stdout
    assert b"Success!" in ret.stdout
    assert b"Failure!" in ret.stdout
    assert b"happy" in ret.stdout
    assert b"sad" in ret.stdout
    assert ret.returncode == 0


def test_sls_tree(runpy, tree):
    cmd = [sys.executable, runpy, "state", "simple", "--tree", tree]
    ret = subprocess.run(cmd, stdout=subprocess.PIPE)
    assert b"True" in ret.stdout
    assert b"Success!" in ret.stdout
    assert b"Failure!" in ret.stdout
    assert b"happy" in ret.stdout
    assert b"sad" in ret.stdout
    assert ret.returncode == 0


def test_fail(runpy, tree):
    cmd = [sys.executable, runpy, "state", "nonexistant.sls"]
    ret = subprocess.run(cmd, stdout=subprocess.PIPE)
    assert ret.returncode != 0
    assert b"Could not find SLS ref nonexistant.sls in sources" in ret.stdout
