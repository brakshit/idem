import json
import subprocess
import sys
import tempfile

import pytest


@pytest.fixture(scope="module")
def cred_file():
    with tempfile.NamedTemporaryFile("w+") as creds:
        creds.write("foo:\n- bar")
        creds.flush()
        yield creds.name


@pytest.fixture(scope="module")
def acct_file():
    with tempfile.NamedTemporaryFile() as f:
        yield f.name


def test_encrypt(runpy, acct_key, cred_file, acct_file):
    proc = subprocess.Popen(
        [
            sys.executable,
            runpy,
            "encrypt",
            cred_file,
            f'--acct-key="{acct_key}"',
            f"--output-file={acct_file}",
            "--crypto-plugin=fernet",
        ],
        env={},
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )

    assert proc.wait() == 0

    stderr = proc.stderr.read().decode()

    assert not stderr, stderr

    with open(acct_file) as fh:
        assert fh.read()


def test_decrypt(runpy, acct_key, acct_file):
    proc = subprocess.Popen(
        [
            sys.executable,
            runpy,
            "decrypt",
            acct_file,
            f"--acct-key='{acct_key}'",
            "--crypto-plugin=fernet",
            "--output=json",
        ],
        env={},
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )

    assert proc.wait() == 0

    stderr = proc.stderr.read().decode()
    assert not stderr, stderr

    stdout = proc.stdout.read().decode()
    assert json.loads(stdout) == {"foo": ["bar"]}, stderr


def test_plaintext(runpy):
    """
    Test that exec modules can run with plaintext credentials
    """
    with tempfile.NamedTemporaryFile("w", suffix=".yaml", delete=True) as creds:
        creds.write("test:\n  default:\n    key: value")
        creds.flush()

        proc = subprocess.Popen(
            [
                sys.executable,
                runpy,
                "exec",
                "test.ctx",
                f"--acct-file={creds.name}",
                "--output=json",
            ],
            env={},
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )

        assert proc.wait() == 0, proc.stderr.read().decode()

        stdout = proc.stdout.read().decode()
        assert json.loads(stdout)["ret"]["acct"] == {"key": "value"}


def test_extras(runpy):
    """
    Test that a json string in extras gets rendered properly
    """
    proc = subprocess.Popen(
        [
            sys.executable,
            runpy,
            "state",
            '--extras={"aws":{"esm":{"region_name":"test"}}}',
            "--output=json",
            "--config-template",
        ],
        env={},
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )

    assert proc.wait() == 0, proc.stderr.read().decode()

    stdout = proc.stdout.read().decode()
    assert json.loads(stdout)["acct"]["extras"] == {
        "aws": {"esm": {"region_name": "test"}}
    }


def test_extras_not_supplied(runpy):
    """
    Test that no value passed to extras becomes a dictionary
    """
    proc = subprocess.Popen(
        [
            sys.executable,
            runpy,
            "state",
            "--output=json",
            "--config-template",
        ],
        env={},
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )

    assert proc.wait() == 0, proc.stderr.read().decode()

    stdout = proc.stdout.read().decode()
    assert json.loads(stdout)["acct"]["extras"] == {}


def test_os_vars(runpy):
    """
    Test that os vars are read properly
    """
    proc = subprocess.Popen(
        [
            sys.executable,
            runpy,
            "decrypt",
            "file",
            "--output=json",
            "--config-template",
        ],
        env={
            "ACCT_FILE": "acct-file",
            "ACCT_KEY": "acct-key",
            "ACCT_PROFILE": "acct-profile",
        },
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )

    assert proc.wait() == 0, proc.stderr.read().decode()

    stdout = proc.stdout.read().decode()
    assert json.loads(stdout)["acct"]["acct_file"] == "acct-file"
    assert json.loads(stdout)["acct"]["acct_key"] == "acct-key"
    assert json.loads(stdout)["idem"]["acct_profile"] == "acct-profile"
