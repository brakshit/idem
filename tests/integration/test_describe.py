import ast
import json
import subprocess
import sys
import tempfile


def test_cli(runpy):
    # Run the describe command
    cmd = [sys.executable, runpy, "describe", "test", "--output=json"]
    assert_describe_output(cmd, runpy, False)


def test_cli_regex(runpy):
    # Run the describe command
    cmd = [sys.executable, runpy, "describe", "te.*", "--output=json"]
    # assert test file
    assert_describe_output(cmd, runpy, True)


def assert_describe_output(cmd, runpy, is_reg_ex):
    ret = subprocess.run(cmd, capture_output=True, encoding="utf-8", env={})
    assert ret.returncode == 0, ret.stderr
    yaml = ret.stdout

    with tempfile.NamedTemporaryFile("w+", suffix=".sls", delete=True) as fh:
        fh.write(yaml)
        fh.flush()

        # Verify that passing states were created from it
        cmd = [
            sys.executable,
            runpy,
            "state",
            fh.name,
            "--output=json",
            "--runtime=serial",
        ]
        ret = subprocess.run(cmd, capture_output=True, encoding="utf-8", env={})
        assert ret.returncode == 0, ret.stderr

    assert ret.stdout
    data = json.loads(ret.stdout)

    assert ret.returncode == 0, ret.stderr
    assert "test_|-Description of test.anop_|-anop_|-anop" in data
    assert (
        "test_|-Description of test.configurable_test_state_|-configurable_test_state_|-configurable_test_state"
        in data
    )
    assert (
        "test_|-Description of test.fail_with_changes_|-fail_with_changes_|-fail_with_changes"
        in data
    )
    assert (
        "test_|-Description of test.fail_without_changes_|-fail_without_changes_|-fail_without_changes"
        in data
    )
    assert "test_|-Description of test.mod_watch_|-mod_watch_|-mod_watch" in data
    assert (
        "test_|-Description of test.none_without_changes_|-none_without_changes_|-none_without_changes"
        in data
    )
    assert "test_|-Description of test.nop_|-nop_|-nop" in data
    assert (
        "test_|-Description of test.succeed_with_changes_|-succeed_with_changes_|-succeed_with_changes"
        in data
    )
    assert (
        "test_|-Description of test.succeed_with_comment_|-succeed_with_comment_|-succeed_with_comment"
        in data
    )
    assert (
        "test_|-Description of test.succeed_without_changes_|-succeed_without_changes_|-succeed_without_changes"
        in data
    )
    assert "test_|-Description of test.unique_op_|-unique_op_|-unique_op" in data
    assert "test_|-Description of test.update_low_|-update_low_|-update_low" in data
    assert "test_|-king_arthur_|-totally_extra_alls_|-nop" in data
    assert "test_|-Description of test.treq_|-treq_|-treq" in data
    # assert describe data from test_regex file.
    if is_reg_ex:
        assert (
            "test_regex_|-Description of regex test_regex.none_without_changes_regex_|-none_without_changes_regex_|-none_without_changes_regex"
            in data
        )
        assert (
            "test_regex_|-Description of regex test_regex.succeed_without_changes_regex_|-succeed_without_changes_regex_|-succeed_without_changes_regex"
            in data
        )

        assert (
            "None describe regular expression!"
            in data[
                "test_regex_|-Description of regex test_regex.none_without_changes_regex_|-none_without_changes_regex_|-none_without_changes_regex"
            ]["comment"]
        )
        assert (
            "Success describe regular expression!"
            in data[
                "test_regex_|-Description of regex test_regex.succeed_without_changes_regex_|-succeed_without_changes_regex_|-succeed_without_changes_regex"
            ]["comment"]
        )


def test_jmespath_output(runpy):
    cmd = [sys.executable, runpy, "describe", "test", "--output=jmespath"]

    ret = subprocess.run(cmd, capture_output=True, check=True, encoding="utf-8", env={})
    assert ret.returncode == 0, ret.stderr
    data = ret.stdout

    assert ast.literal_eval(data) == [
        {
            "name": "Description of test.acct",
            "ref": "test.acct",
            "resource": [{"name": "acct"}],
        },
        {
            "name": "Description of test.anop",
            "ref": "test.anop",
            "resource": [{"name": "anop"}],
        },
        {
            "name": "Description of test.configurable_test_state",
            "ref": "test.configurable_test_state",
            "resource": [
                {"name": "configurable_test_state"},
                {"changes": True},
                {"result": True},
                {"comment": ""},
            ],
        },
        {
            "name": "Description of test.fail_with_changes",
            "ref": "test.fail_with_changes",
            "resource": [{"name": "fail_with_changes"}],
        },
        {
            "name": "Description of test.fail_without_changes",
            "ref": "test.fail_without_changes",
            "resource": [{"name": "fail_without_changes"}],
        },
        {
            "name": "Description of test.mod_watch",
            "ref": "test.mod_watch",
            "resource": [{"name": "mod_watch"}],
        },
        {
            "name": "Description of test.none_without_changes",
            "ref": "test.none_without_changes",
            "resource": [{"name": "none_without_changes"}],
        },
        {
            "name": "Description of test.nop",
            "ref": "test.nop",
            "resource": [{"name": "nop"}],
        },
        {
            "name": "Description of test.parallel_state",
            "ref": "test.parallel_state",
            "resource": [{"name": "parallel_state"}],
        },
        {
            "name": "Description of test.present_resource",
            "ref": "test.present_resource",
            "resource": [
                {"name": "present_resource"},
                {"enforce_state": None},
                {"remote_state": None},
                {"resource_id": None},
                {"result": True},
            ],
        },
        {
            "name": "Description of test.succeed_with_changes",
            "ref": "test.succeed_with_changes",
            "resource": [{"name": "succeed_with_changes"}],
        },
        {
            "name": "Description of test.succeed_with_comment",
            "ref": "test.succeed_with_comment",
            "resource": [{"name": "succeed_with_comment"}, {"comment": None}],
        },
        {
            "name": "Description of test.succeed_without_changes",
            "ref": "test.succeed_without_changes",
            "resource": [{"name": "succeed_without_changes"}],
        },
        {
            "name": "Description of test.treq",
            "ref": "test.treq",
            "resource": [{"name": "treq"}],
        },
        {
            "name": "Description of test.unique_op",
            "ref": "test.unique_op",
            "resource": [{"name": "unique_op"}],
        },
        {
            "name": "Description of test.update",
            "ref": "test.update",
            "resource": [
                {"name": "update"},
                {"param_1": None},
                {"param_2": None},
                {"result": True},
            ],
        },
        {
            "name": "Description of test.update_low",
            "ref": "test.update_low",
            "resource": [{"name": "update_low"}],
        },
    ]


def test_jmespath_filter(runpy):
    cmd = [
        sys.executable,
        runpy,
        "describe",
        "test",
        "--output=jmespath",
        "--filter",
        "@",
    ]

    ret = subprocess.run(cmd, capture_output=True, check=True, encoding="utf-8", env={})
    assert ret.returncode == 0, ret.stderr
    data = ret.stdout

    assert ast.literal_eval(data) == [
        {
            "name": "Description of test.acct",
            "ref": "test.acct",
            "resource": [{"name": "acct"}],
        },
        {
            "name": "Description of test.anop",
            "ref": "test.anop",
            "resource": [{"name": "anop"}],
        },
        {
            "name": "Description of test.configurable_test_state",
            "ref": "test.configurable_test_state",
            "resource": [
                {"name": "configurable_test_state"},
                {"changes": True},
                {"result": True},
                {"comment": ""},
            ],
        },
        {
            "name": "Description of test.fail_with_changes",
            "ref": "test.fail_with_changes",
            "resource": [{"name": "fail_with_changes"}],
        },
        {
            "name": "Description of test.fail_without_changes",
            "ref": "test.fail_without_changes",
            "resource": [{"name": "fail_without_changes"}],
        },
        {
            "name": "Description of test.mod_watch",
            "ref": "test.mod_watch",
            "resource": [{"name": "mod_watch"}],
        },
        {
            "name": "Description of test.none_without_changes",
            "ref": "test.none_without_changes",
            "resource": [{"name": "none_without_changes"}],
        },
        {
            "name": "Description of test.nop",
            "ref": "test.nop",
            "resource": [{"name": "nop"}],
        },
        {
            "name": "Description of test.parallel_state",
            "ref": "test.parallel_state",
            "resource": [{"name": "parallel_state"}],
        },
        {
            "name": "Description of test.present_resource",
            "ref": "test.present_resource",
            "resource": [
                {"name": "present_resource"},
                {"enforce_state": None},
                {"remote_state": None},
                {"resource_id": None},
                {"result": True},
            ],
        },
        {
            "name": "Description of test.succeed_with_changes",
            "ref": "test.succeed_with_changes",
            "resource": [{"name": "succeed_with_changes"}],
        },
        {
            "name": "Description of test.succeed_with_comment",
            "ref": "test.succeed_with_comment",
            "resource": [{"name": "succeed_with_comment"}, {"comment": None}],
        },
        {
            "name": "Description of test.succeed_without_changes",
            "ref": "test.succeed_without_changes",
            "resource": [{"name": "succeed_without_changes"}],
        },
        {
            "name": "Description of test.treq",
            "ref": "test.treq",
            "resource": [{"name": "treq"}],
        },
        {
            "name": "Description of test.unique_op",
            "ref": "test.unique_op",
            "resource": [{"name": "unique_op"}],
        },
        {
            "name": "Description of test.update",
            "ref": "test.update",
            "resource": [
                {"name": "update"},
                {"param_1": None},
                {"param_2": None},
                {"result": True},
            ],
        },
        {
            "name": "Description of test.update_low",
            "ref": "test.update_low",
            "resource": [{"name": "update_low"}],
        },
    ]


def test_filter(runpy):
    cmd = [
        sys.executable,
        runpy,
        "describe",
        "test",
        "--output=jmespath",
        "--filter",
        "[?resource[?name=='succeed_with_comment']]",
    ]

    ret = subprocess.run(cmd, capture_output=True, check=True, encoding="utf-8", env={})
    assert ret.returncode == 0, ret.stderr
    data = ret.stdout

    assert ast.literal_eval(data) == [
        {
            "name": "Description of test.succeed_with_comment",
            "resource": [{"name": "succeed_with_comment"}, {"comment": None}],
            "ref": "test.succeed_with_comment",
        },
    ], data


def test_describe_error(runpy, tests_dir):
    cmd = [sys.executable, runpy, "state", tests_dir / "sls" / "describe.sls"]
    ret = subprocess.run(cmd, capture_output=True, encoding="utf-8", env={})
    assert ret.returncode, ret.stderr
    assert (
        "'describe' functions should only be called by the describe subcommand."
        in ret.stdout
    )
