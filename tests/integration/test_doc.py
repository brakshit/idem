import json
import subprocess
import sys


def test_exec_doc(tree, runpy):
    ref = "exec.test.ping"
    cmd = [sys.executable, runpy, "doc", ref, "--output=json"]
    ret = subprocess.run(cmd, env={}, capture_output=True, encoding="utf-8", check=True)
    data = json.loads(ret.stdout)

    assert data[ref]["doc"] == "Immediately return success"
    assert data[ref]["file"].endswith("exec/test.py"), data[ref]["file"]
    assert data[ref]["ref"] == ref
    assert "parameters" in data[ref]
    assert "contracts" in data[ref]
    assert "start_line_number" in data[ref]
    assert "end_line_number" in data[ref]


def test_state_doc(tree, runpy):
    ref = "states.test.present"
    cmd = [sys.executable, runpy, "doc", ref, "--output=json"]
    ret = subprocess.run(cmd, env={}, capture_output=True, encoding="utf-8", check=True)
    data = json.loads(ret.stdout)

    assert (
        data[ref]["doc"]
        == "Return the previous old_state, if it's not specified in sls, and the given new_state.\nRaise an error on fail"
    )
    assert data[ref]["file"].endswith("states/test.py"), data[ref]["file"]
    assert data[ref]["ref"] == ref
    assert "parameters" in data[ref]
    assert "contracts" in data[ref]
    assert "start_line_number" in data[ref]
    assert "end_line_number" in data[ref]


def test_full_doc(tree, runpy):
    cmd = [sys.executable, runpy, "doc", "--output=json"]
    ret = subprocess.run(cmd, env={}, capture_output=True, encoding="utf-8", check=True)
    data = json.loads(ret.stdout)

    # We're not going to verify the documentation of every single function on the hub
    # It's enough that the command was successful and returned more than one thing
    assert len(data) > 1
