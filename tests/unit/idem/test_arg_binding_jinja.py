import pytest


@pytest.mark.asyncio
async def test_jinja_arg_binding(mock_hub, hub):
    """
    test rend.jinja.render renders arg-binding template correctly
    """
    prepare_mock_hub(mock_hub, hub)
    add_data_to_hub(mock_hub, hub)

    byte_data = b"resource_ids: {{ hub.idem.arg_bind.resolve('${test:multi_result_new_state}').values() | map(attribute='resource_id') | list }}"

    ret = await mock_hub.rend.jinja.render(byte_data)
    assert ret

    assert "resource_ids: ['HZ1', 'HZ2', 'HZ3']" == ret


@pytest.mark.asyncio
async def test_arg_binding_with_no_data_to_resolve(mock_hub, hub):
    """
    test rend.jinja.render should throw rend error as arg-binding couldnt be resolved
    """
    prepare_mock_hub(mock_hub, hub)

    byte_data = (
        b"subnet_list: [{% for k,v in  hub.idem.arg_bind.resolve('${test:multi_result_new_state}').items() %} {{ v['resource_id'] }} {% "
        b"endfor %}] "
    )

    with pytest.raises(Exception) as exc:
        await mock_hub.rend.jinja.render(byte_data)

    assert (
        f"Arg_bind template '{'${test:multi_result_new_state}'}' could not be resolved to any value"
        in exc.value.args[0]
    )


@pytest.mark.asyncio
async def test_multiple_occurence_arg_binding(mock_hub, hub):
    """
    test rend.jinja.render renders multiple instances of same arg-binding with same value
    """
    prepare_mock_hub(mock_hub, hub)
    add_data_to_hub(mock_hub, hub)
    byte_data = (
        b"subnet_list_1: [{% for k,v in  hub.idem.arg_bind.resolve('${test:multi_result_new_state}').items() %} {{ v['resource_id'] }} "
        b"{% endfor %}]\n"
        b"subnet_list_2: [{% for k,v in  hub.idem.arg_bind.resolve('${test:multi_result_new_state}').items() %} {{ v['resource_id'] }} "
        b"{% endfor %}]"
    )

    ret = await mock_hub.rend.jinja.render(byte_data)
    assert ret

    assert "subnet_list_1: [ HZ1  HZ2  HZ3 ]\nsubnet_list_2: [ HZ1  HZ2  HZ3 ]" == ret


def add_data_to_hub(mock_hub, hub):
    low_data = [
        {
            "state": "test",
            "name": "multi_result_new_state",
            "__sls__": "arg_bind_in_jinja",
            "__id__": "multi_result_new_state",
            "order": 100000,
            "fun": "succeed_with_multiple_results",
            "ctx": {
                "run_name": "test",
                "test": False,
                "tag": "test_|-multi_result_new_state_|-multi_result_new_state_|-succeed_with_multiple_results",
                "acct": {},
                "old_state": None,
            },
        }
    ]
    mock_hub.idem.RUNS = {"test": {"low": low_data}}

    mock_hub.idem.RUNS["test"]["running"] = {
        "test_|-multi_result_new_state_|-multi_result_new_state_|-succeed_with_multiple_results": {
            "tag": "test_|-multi_result_new_state_|-multi_result_new_state_|-succeed_with_multiple_results",
            "name": "multi_result_new_state",
            "changes": {
                0: {
                    "resource_id": "HZ1",
                    "key_with_nested_value": {"name": "nested_value1"},
                },
                1: {
                    "resource_id": "HZ2",
                    "key_with_nested_value": {"name": "nested_value2"},
                },
                2: {
                    "resource_id": "HZ3",
                    "key_with_nested_value": {"name": "nested_value3"},
                },
            },
            "new_state": {
                0: {
                    "resource_id": "HZ1",
                    "key_with_nested_value": {"name": "nested_value1"},
                },
                1: {
                    "resource_id": "HZ2",
                    "key_with_nested_value": {"name": "nested_value2"},
                },
                2: {
                    "resource_id": "HZ3",
                    "key_with_nested_value": {"name": "nested_value3"},
                },
            },
            "old_state": {},
            "comment": "Success!",
            "result": True,
            "esm_tag": "test_|-multi_result_new_state_|-multi_result_new_state_|-",
            "__run_num": 1,
        }
    }


def prepare_mock_hub(mock_hub, hub):
    mock_hub.pop.sub.add(dyne_name="idem")
    mock_hub.idem.RUN_NAME = hub.idem.RUN_NAME = "test"
    mock_hub.idem.tools.gen_tag = hub.idem.tools.gen_tag
    mock_hub.rend.jinja.render = hub.rend.jinja.render
    mock_hub.idem.arg_bind.resolve = hub.idem.arg_bind.resolve
    mock_hub.tool.idem.arg_bind_utils.parse_index = (
        hub.tool.idem.arg_bind_utils.parse_index
    )
    mock_hub.tool.idem.arg_bind_utils.find_arg_reference_data = (
        hub.tool.idem.arg_bind_utils.find_arg_reference_data
    )
