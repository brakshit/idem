pre:
  test.nop:
    - prereq:
        - test: change_1

change_1:
  test.succeed_with_changes:
    - require:
        - test: good

good:
  test.nop
