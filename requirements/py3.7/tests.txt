#
# This file is autogenerated by pip-compile
# To update, run:
#
#    pip-compile --output-file=requirements/py3.7/tests.txt requirements/tests.in
#
-e file:.#egg=idem
    # via
    #   -r requirements/tests.in
    #   pytest-idem
acct==8.3.0
    # via
    #   -r requirements/base.txt
    #   evbus-kafka
    #   evbus-pika
    #   idem
    #   pop-evbus
aio-pika==6.8.1
    # via evbus-pika
aiofiles==0.7.0
    # via
    #   -r requirements/base.txt
    #   acct
    #   dict-toolbox
    #   idem
    #   pop-tree
aiokafka==0.7.2
    # via evbus-kafka
aiormq==3.3.1
    # via aio-pika
altgraph==0.17.2
    # via pyinstaller
asynctest==0.13.0
    # via
    #   -r requirements/tests.in
    #   pytest-pop
attrs==21.2.0
    # via pytest
cffi==1.14.5
    # via cryptography
colored==1.4.2
    # via rend
cryptography==3.4.7
    # via acct
dict-toolbox==2.2.0
    # via
    #   -r requirements/base.txt
    #   acct
    #   idem
    #   pop
    #   pop-config
    #   pytest-pop
    #   rend
evbus-kafka==3.0.1
    # via -r requirements/extra/kafka.txt
evbus-pika==3.0.1
    # via -r requirements/extra/rabbitmq.txt
idna==3.3
    # via yarl
importlib-metadata==4.3.0
    # via
    #   pluggy
    #   pyinstaller
    #   pytest
iniconfig==1.1.1
    # via pytest
jinja2==3.0.2
    # via
    #   -r requirements/base.txt
    #   idem
    #   rend
jmespath==0.10.0
    # via
    #   -r requirements/base.txt
    #   idem
kafka-python==2.0.2
    # via aiokafka
lazy-object-proxy==1.7.1
    # via pop
markupsafe==2.0.1
    # via jinja2
mock==4.0.3
    # via
    #   -r requirements/tests.in
    #   pytest-pop
msgpack==1.0.2
    # via
    #   dict-toolbox
    #   pop-evbus
    #   pop-serial
multidict==5.2.0
    # via yarl
nest-asyncio==1.5.1
    # via
    #   pop-loop
    #   pytest-pop
packaging==20.9
    # via pytest
pamqp==2.3.0
    # via aiormq
pluggy==0.13.1
    # via pytest
pop-config==9.0.0
    # via
    #   -r requirements/base.txt
    #   acct
    #   idem
    #   pop
    #   pytest-pop
pop-evbus==6.1.0
    # via
    #   -r requirements/base.txt
    #   evbus-kafka
    #   evbus-pika
    #   idem
pop-loop==1.0.4
    # via
    #   -r requirements/base.txt
    #   idem
    #   pop
pop-serial==1.1.0
    # via
    #   -r requirements/base.txt
    #   acct
    #   idem
    #   pop-evbus
pop-tree==9.2.1
    # via
    #   -r requirements/base.txt
    #   idem
pop==21.0.2
    # via
    #   -r requirements/base.txt
    #   acct
    #   evbus-kafka
    #   evbus-pika
    #   idem
    #   pop-config
    #   pop-evbus
    #   pop-loop
    #   pop-serial
    #   pop-tree
    #   pytest-pop
    #   rend
py==1.10.0
    # via pytest
pycparser==2.20
    # via cffi
pyinstaller-hooks-contrib==2022.3
    # via pyinstaller
pyinstaller==5.0.1
    # via
    #   -r requirements/package.in
    #   tiamat-pip
pyparsing==2.4.7
    # via packaging
pytest-async==0.1.1
    # via pytest-pop
pytest-asyncio==0.15.1
    # via pytest-pop
pytest-idem==2.0.1
    # via -r requirements/tests.in
pytest-pop==9.0.0
    # via
    #   -r requirements/tests.in
    #   pytest-idem
pytest==6.2.4
    # via
    #   pytest-asyncio
    #   pytest-pop
pyyaml==6.0
    # via
    #   -r requirements/base.txt
    #   acct
    #   dict-toolbox
    #   idem
    #   pop
    #   rend
rend==6.4.2
    # via
    #   -r requirements/base.txt
    #   acct
    #   idem
    #   pop-config
    #   pop-evbus
    #   pop-tree
sniffio==1.2.0
    # via pop-loop
tiamat-pip==1.5.1
    # via -r requirements/package.in
toml==0.10.2
    # via
    #   -r requirements/base.txt
    #   idem
    #   pytest
    #   rend
tqdm==4.62.3
    # via
    #   -r requirements/base.txt
    #   idem
typing-extensions==3.10.0.0
    # via
    #   importlib-metadata
    #   tiamat-pip
    #   yarl
wheel==0.36.2
    # via
    #   -r requirements/base.txt
    #   idem
yarl==1.7.2
    # via
    #   aio-pika
    #   aiormq
zipp==3.4.1
    # via importlib-metadata

# The following packages are considered to be unsafe in a requirements file:
# pip
# setuptools
