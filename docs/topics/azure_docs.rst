==============================
Microsoft Azure Cloud Provider
==============================

Setting up cloud resources using Idem is easy to do! Check out the documentation
at the link below:

`Azure for Idem (idem-azurerm) Documentation
<https://idem-azurerm.readthedocs.io/en/latest/>`_
