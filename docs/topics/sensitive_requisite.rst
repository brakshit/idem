===========================
Sensitive Requisite
===========================

Sensitive requisite can be used in sls blocks to prevented parameters to be outputted to console.

For Idem state resources that implement the "resource" contract, parameters specified under the sensitive requisite are hidden from the "changes" output.

In the following example, State_A uses sensitive requisite to hide its secret from "changes" output.

.. code-block:: sls

   State_A:
      cloud.instance.present:
        - name: my-resource
        - public: public-data
        - secret: secret-data
        - sensitive:
          - secret

Assume present() does a creating operation, the console output of this Idem run will be:

.. code-block:: sls

   changes:
      new:
        name: my-resource
        public: public-data

Note: sensitive requisite only hides data in "changes" that is outputted to console. All data will still be saved into ESM cache in plain text.
